Basics of RDD  : 1 PD 

NOTE :  Create individual RDDs wherever required in the following process. 

consider the following text file. 

Priority,qty,sales
Low,6,261.54
High,44,10123.02
High,27,244.57
High,30,4965.75
Null,22,394.27
Null,21,146.69
High,12,93.54
High,22,905.08
High,21,2781.82
Low,44,228.41

1.
i) Upload the data using RDD.
var df = spark.read.format("csv").option("header", "true").load("rdd_basics.txt")
ii) Sort given data by priority
df.sort("Priority")
2. 
i) Place Low and high priority data into 2 seperate files.

var df_high = df.filter('Priority=High')
df_high.rdd.saveAsTextFile("/tmp/df_high")
var df_low = df.filter('Priority=Low')
df_low.rdd.saveAsTextFile("/tmp/df_low")

ii) Place records with less than 20 as qty into another file.
var df_20 = df.filter("qty<20")
df_20.rdd.saveAsTextFile("/tmp/df_less20")

3. 
i) Create  RDDs for priority & sales, qty & sales .
var df_ps = df.select("Priority", "sales").rdd
var df_qs = df.select("qty", "sales").rdd

ii) Load sum of the sales aggregated by Priority into another file.
df.select("Priority", "sales").groupBy("Priority").agg(sum("sales").alias("sales")).write.format("parquet").save("/tmp/df_agg.parquet")
var df_agg = spark.read.load("/tmp/df_agg.parquet")

4. 
i) Load avg of the sales aggregated by qty into another file. 

df.select("qty", "sales").groupBy("qty").agg(avg("sales").alias("sales")).write.format("parquet").save("/tmp/df_agg2.parquet")
var df_agg2 = spark.read.load("/tmp/df_agg2.parquet")

ii) Display count of records in low and high priorities .

df.select("Priority").groupBy("Priority").count().filter("Priority like 'High' or Priority like 'Low'").show

5. 
i) Load above data into an RDD with 4 partitions
df.select("Priority").groupBy("Priority").count().filter("Priority like 'High' or Priority like 'Low'").repartition(4)
??

ii) Display data along with partitions.
df.select("Priority").groupBy("Priority").count().filter("Priority like 'High' or Priority like 'Low'").repartition(4).rdd.map(x => x).collect

6.
i) Re-partition data to 6 partitions.
df.select("Priority").groupBy("Priority").count().filter("Priority like 'High' or Priority like 'Low'").repartition(6).rdd.getNumPartitions

ii) Display whole data from all partitions and load into another text file. 
7.  Consider the following text file.
priority,grade
Low,A
High,B
Null,K

i) Develop a RDD for above data with 2 partitions and display.

import org.apache.spark.sql.Dataset
val csvData: Dataset[String] = spark.sparkContext.parallelize(
  """
    |priority,grade
    |Low,A
    |High,B
    |Null,K
  """.stripMargin.lines.toList).toDS()
val frame = spark.read.option("header", true).option("inferSchema",true).csv(csvData).repartition(2).show

ii) List out  priority,quantity,sales ,grade and save into another file. 
?

8.
i) From above file, separate data by grades and load into 3 different files. 
ii) Is it possible to lessen the number of partitions? verify re-partition with above file by decreasing to 1 partition.
Yes. Use df.repartition(1)